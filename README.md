# Limit trader
Enters limit buy/sell orders for you at (almost) the current price on the GDAX exchange, to avoid commissions on taker orders

![Screenshot](screenshot.png)

## Cost Savings
GDAX charges a 0.25% or 0.3% fee on taker orders (market orders, and limit orders that cross the bid-ask spread).
However, there is no fee on maker orders (limit orders that do not cross the bid-ask spread).

This application will enter limit orders for you at 1 cent (or 0.00001 BTC) above/below the bid/ask price.
This provides a reasonable chance of the order executing, but often prevents fees.

There is a chance that an order will not execute, because the price changed while the order was being entered.
There is also a chance, if the price goes in the other direction, that the order is executed as a maker order.

Thus, there is no guarantee of order execution or of prevention of fees.

All trading pairs available on GDAX are supported (BTC, BCH, ETH, LTC, etc.).

## How to use
First, set up API access (see Setup section below).

Download the [JAR](trader.jar) (click the 'View raw' button after clicking the link) and run it.

Choose the trading pair from the choices at the top.

Choose whether to place market or limit orders market orders will always execute and will always be subject to the fee).

Choose whether to buy or sell. The buttons will buy/sell all, half, or 1/4th of the available funds.
Currently, there is no way to enter a precise amount to buy or sell.

The cancel order button will cancel all outstanding orders. Market orders are always fulfilled instantly and cannot be cancelled.

## Warnings
GDAX has limitations on how small of an order can be placed.
This application will not warn you if the order you are trying to place is too small.
It will silently fail.

It is not possible to truly buy or sell 100% with limit orders.
If you have $100 in your account and enter a market order for 100%, the application will enter an order for $100. GDAX will automatically deduct the fee from this, giving you $99.75 worth of coin.

However, with limit orders, it is the application's job to account for the fee. Entering a limit order for 100% will actually only enter a limit order for $99.75 (99.75*1.0025=100), and the remaining $0.25 will be kept in case a fee is applied.
If a fee is applied to the limit order, you will lose $100 USD and get $99.75 of coin. If there is no fee on the order, you will lose $99.75 USD and get $99.75 of coin, with $0.25 USD left in your account.

## Known Bugs
Accounts may occasionally stop updating after multiple buys/sells (the amounts shown for USD/BTC available in your account will not update to reflect the latest status).
Restarting the program will fix this. Still TBD whether new orders will go through after this happens.

## Keyboard Shortcuts
Market/Limit orders: M / L

Buy 100/50/25: Double tap Q, W, E, respectively

Sell 100/50/25: Double tap A, S, D, respectively

Cancel all pending orders: C

## Setup
Ensure you have [Java version 9](http://www.oracle.com/technetwork/java/javase/downloads/jre9-downloads-3848532.html) or newer installed on your computer. (Download the .exe for Windows, or the .dmg for macOS).

Go to the [GDAX API Page](https://www.gdax.com/settings/api).
Create a new API Key, with permission to View and Trade.

You can leave the passphrase at the default value, or enter one manually.

Finally, the key, secret, and passphrase must be saved onto your computer.
Either download and run the [setup application](traderSetup.jar) (click the 'View raw' button after clicking the link), or perform it manually.

### Manual Setup:
The file with the key, secret, and passphrase must have the following contents.

```
APIKEY=<paste key here>
APISECRET=<paste secret here>
APIPASSPHRASE=<paste passphrase here>
<empty line>
```

Make sure to leave an empty line at the end of the file.

#### Windows:
Open Notepad. After entering the file contents, `Save As`. Navigate to the folder `C:\Users\yourWindowsUsername`.

Save the file as `.limitTraderKey`.

In the `Save as type` popup menu, choose `All Files`. Ensure there is not a `.txt` at the end of the filename.

#### Mac/Linux:
Open a terminal. `nano ~/.limitTraderKey`.

Enter the file contents.

`Ctrl-X` to save, `Y` to confirm.

##### A Note for Linux
On Linux, ensure you have JavaFX for Java 9.
OpenJDK does not include this by default, so install it from your distribution's repositories or use the Oracle JRE.
