/*
 * Copyright 2018 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.trader;

import com.subraizada.trader.controller.RootViewController;
import com.subraizada.trader.util.ApiGdaxWebSocket;
import java.util.ArrayList;
import java.util.concurrent.ScheduledExecutorService;
import java.util.prefs.Preferences;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Trader extends Application {

	public static final boolean TEST_MODE = false; // use sandbox or actual APIs

	public static String INITIAL_TICKER;

	public static ArrayList<ScheduledExecutorService> services = new ArrayList<>(2);

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		INITIAL_TICKER = Preferences.userNodeForPackage(Trader.class).get("product", "BTC-USD");

		FXMLLoader loader = new FXMLLoader(getClass().getResource("view/rootView.fxml"));

		//Parent root = FXMLLoader.load(getClass().getResource("view/rootView.fxml"));

		Parent root = loader.load();

		primaryStage.setTitle("Limit Trader");
		primaryStage.setScene(new Scene(root));
		primaryStage.sizeToScene();
		primaryStage.setResizable(false);
		primaryStage.show();

		RootViewController controller = loader.getController();
		controller.setKeybindings(); // needs to be done after primaryStage.show();
	}

	@Override
	public void stop() throws Exception {
		ApiGdaxWebSocket.exit();
		for (ScheduledExecutorService service : services)
			service.shutdownNow();
		super.stop();
	}
}
