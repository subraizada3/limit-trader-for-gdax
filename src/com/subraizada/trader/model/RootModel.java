/*
 * Copyright 2018 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.trader.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public class RootModel {
	private ArrayList<String> tradingPairs;
	private String currentProduct;
	private boolean makeMarketOrders = false;
	private HashMap<String, BigDecimal> accounts;

	public ArrayList<String> getTradingPairs() {
		return tradingPairs;
	}

	public void setTradingPairs(ArrayList<String> tradingPairs) {
		this.tradingPairs = tradingPairs;
	}

	public String getCurrentProduct() {
		return currentProduct;
	}

	public void setCurrentProduct(String currentProduct) {
		this.currentProduct = currentProduct;
	}

	public boolean isMakeMarketOrders() {
		return makeMarketOrders;
	}

	public void setMakeMarketOrders(boolean makeMarketOrders) {
		this.makeMarketOrders = makeMarketOrders;
	}

	public HashMap<String, BigDecimal> getAccounts() {
		return accounts;
	}

	public void setAccounts(HashMap<String, BigDecimal> accounts) {
		this.accounts = accounts;
	}
}
