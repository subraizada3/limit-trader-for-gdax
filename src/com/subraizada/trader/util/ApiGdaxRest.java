/*
 * Copyright 2018 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.trader.util;

import com.subraizada.trader.Trader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

// REST api used to get account information ($ available to trade with), and to make trades
public class ApiGdaxRest {

	private static final String API_BASE_URL_PROD = "https://api.gdax.com";
	private static final String API_BASE_URL_TEST = "https://api-public.sandbox.gdax.com";
	private static final String API_BASE_URL = Trader.TEST_MODE ? API_BASE_URL_TEST : API_BASE_URL_PROD;

	public static ArrayList<String> getProducts() throws IOException {
		String jsonString = IOUtils.toString(new URL(API_BASE_URL + "/products"), StandardCharsets.UTF_8);
		JSONArray rootJsonArray = new JSONArray(jsonString);

		ArrayList<String> tradingPairs = new ArrayList<>(12);

		for (Object obj : rootJsonArray) {
			tradingPairs.add(((JSONObject) obj).getString("id"));
		}

		return tradingPairs;
	}

	public static void orderMarket(String ticker, boolean buy, BigDecimal amount) throws NoSuchAlgorithmException, InvalidKeyException, IOException {
		JSONObject json = new JSONObject();
		json.put("type", "market");
		json.put("side", buy ? "buy" : "sell");
		json.put("product_id", ticker);
		//json.put("funds", amount.toPlainString());
		if (buy) {
			json.put("funds", amount.toPlainString());
		} else {
			json.put("size", amount.toPlainString());
		}
		System.out.println(json);
		doAuthenticatedRequest("/orders", "POST", json.toString());
	}

	public static void orderLimit(String ticker, boolean buy, BigDecimal amount, BigDecimal price) throws NoSuchAlgorithmException, InvalidKeyException, IOException {
		JSONObject json = new JSONObject();
		json.put("type", "limit");
		json.put("side", buy ? "buy" : "sell");
		json.put("product_id", ticker);
		json.put("price", price.toPlainString());
		json.put("size", amount.toPlainString());
		System.out.println(json);
		doAuthenticatedRequest("/orders", "POST", json.toString());
	}

	public static Quote quote(String tradingPair) throws IOException {
		String jsonString = IOUtils.toString(new URL(API_BASE_URL + "/products/" + tradingPair + "/ticker"), StandardCharsets.UTF_8);
		JSONObject obj = new JSONObject(jsonString);

		BigDecimal price = new BigDecimal(obj.getString("price"));
		BigDecimal bid = new BigDecimal(obj.getString("bid"));
		BigDecimal ask = new BigDecimal(obj.getString("ask"));

		return new Quote(price, bid, ask, ask.subtract(bid));
	}

	// get trading accounts & the amount of money in them
	// String is currency, BigDecimal is value
	// rounds down to the precision specified in the Currencies class
	public static HashMap<String, BigDecimal> getAccounts() {
		String response = "";

		try {
			response = doAuthenticatedRequest("/accounts", "GET", "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		HashMap<String, BigDecimal> accounts = new HashMap<>();

		JSONArray array = new JSONArray(response);
		for (Object arrayObj : array) {
			JSONObject obj = (JSONObject) arrayObj;
			String currency = obj.getString("currency");

			// trim currency to correct # of decimal places, round down
			int decimalPlaces = Currencies.getDecimalPlaces(currency);
			BigDecimal availableCash = new BigDecimal(obj.getString("available"));
			availableCash = availableCash.setScale(decimalPlaces, RoundingMode.DOWN);
			// add to HashMap
			accounts.put(currency, availableCash);
		}

		return accounts;
	}

	public static void cancelAllOrders() {
		try {
			doAuthenticatedRequest("/orders", "DELETE", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int getPendingOrders() {
		try {
			String jsonString = doAuthenticatedRequest("/orders", "GET", "");
			return new JSONArray(jsonString).length();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return -1;
	}

	private static String doAuthenticatedRequest(String requestPath, String httpRequestType, String message) throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		// very large credit to https://stackoverflow.com/a/48382532 for making this finally work
		String currentTimeString = String.valueOf(Instant.now().getEpochSecond());

		URL url = new URL(API_BASE_URL + requestPath);

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		connection.setRequestProperty("CB-ACCESS-KEY", ApiGdaxKey.getKey());
		connection.setRequestProperty("CB-ACCESS-SIGN", signMessage(currentTimeString, httpRequestType, requestPath, message));
		connection.setRequestProperty("CB-ACCESS-TIMESTAMP", currentTimeString);
		connection.setRequestProperty("CB-ACCESS-PASSPHRASE", ApiGdaxKey.getPassphrase());
		connection.setRequestProperty("content-type", "application/json");
		connection.setRequestProperty("Accept", "application/json");

		connection.setRequestMethod(httpRequestType);
		connection.setConnectTimeout(5000);
		connection.setReadTimeout(5000);

		if (httpRequestType.equals("POST")) {
			connection.setDoOutput(true);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(message);
			out.flush();
		}

		// get the response from the server, and return it (if HTTP 200), or print it to syserr
		String status = connection.getResponseMessage();

		BufferedReader br;
		if (status.equals("OK"))
			br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		else br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));

		String inputLine;
		StringBuilder response = new StringBuilder();
		while ((inputLine = br.readLine()) != null) response.append(inputLine);
		br.close();
		connection.disconnect();

		String responseStr = response.toString();

		if (status.equals("OK")) {
			System.out.println(responseStr);
			return responseStr;
		} else {
			System.err.println(responseStr);
			throw new IOException("Got bad response from API - HTTP " + connection.getResponseCode());
		}
	}

	private static String signMessage(String currentTimeString, String httpRequestType, String requestPath, String message) throws NoSuchAlgorithmException, InvalidKeyException {
		String toHash = currentTimeString + httpRequestType + requestPath + message;
		Mac sha256Mac = Mac.getInstance("HmacSHA256");
		byte[] secret = Base64.getDecoder().decode(ApiGdaxKey.getSecret());
		SecretKeySpec secretKey = new SecretKeySpec(secret, "HmacSHA256");
		sha256Mac.init(secretKey);
		return Base64.getEncoder().encodeToString(sha256Mac.doFinal(toHash.getBytes()));
	}
}
