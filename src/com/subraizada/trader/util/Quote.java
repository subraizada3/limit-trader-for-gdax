/*
 * Copyright 2018 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.trader.util;

import java.math.BigDecimal;

public class Quote {
	private final BigDecimal price, bid, ask, spread;

	public Quote(BigDecimal price, BigDecimal bid, BigDecimal ask, BigDecimal spread) {
		this.price = price;
		this.bid = bid;
		this.ask = ask;
		this.spread = spread;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public BigDecimal getBid() {
		return bid;
	}

	public BigDecimal getAsk() {
		return ask;
	}

	public BigDecimal getSpread() {
		return spread;
	}
}
