/*
 * Copyright 2018 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.trader.util;

import com.subraizada.trader.Trader;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONObject;

// WebSocket API used to get up-to-date price information
// limited to following one trading product at a time
public class ApiGdaxWebSocket {

	private static final String API_BASE_URL_PROD = "wss://ws-feed.gdax.com/";
	private static final String API_BASE_URL_TEST = "wss://ws-feed-public.sandbox.gdax.com/";
	private static final String API_BASE_URL = Trader.TEST_MODE ? API_BASE_URL_TEST : API_BASE_URL_PROD;

	private static String currentProduct = Trader.INITIAL_TICKER; // needed when unsubscribing from a channel

	private static QuoteConsumer updateCallback;

	private static WebSocketClient socket;

	public static void initialize(QuoteConsumer updateCallback) throws InterruptedException {
		ApiGdaxWebSocket.updateCallback = updateCallback;
		try {
			socket = new WebSocketClient(new URI(API_BASE_URL)) {
				@Override
				public void onOpen(ServerHandshake handshakeData) {
					subscribe(Trader.INITIAL_TICKER);
				}

				@Override
				public void onMessage(String message) {
					ApiGdaxWebSocket.onMessage(message);
				}

				@Override
				public void onClose(int code, String reason, boolean remote) {
				}

				@Override
				public void onError(Exception e) {
					e.printStackTrace(System.out);
				}
			};
			socket.connectBlocking();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	public static void subscribe(String product) {
		JSONObject sendObj = new JSONObject();

		sendObj.put("type", "subscribe");

		JSONArray productsArray = new JSONArray();
		productsArray.put(product.toUpperCase());
		sendObj.put("product_ids", productsArray);

		JSONArray channelsArray = new JSONArray();
		channelsArray.put("ticker");
		sendObj.put("channels", channelsArray);

		socket.send(sendObj.toString());

		currentProduct = product;
	}

	public static void unsubscribeFromCurrentProduct() {
		// exact same message as subscribe(product), but "type" is "unsubscribe", not "subscribe"
		JSONObject sendObj = new JSONObject();

		sendObj.put("type", "unsubscribe");

		JSONArray productsArray = new JSONArray();
		productsArray.put(currentProduct.toUpperCase());
		sendObj.put("product_ids", productsArray);

		JSONArray channelsArray = new JSONArray();
		channelsArray.put("ticker");
		sendObj.put("channels", channelsArray);

		socket.send(sendObj.toString());

		currentProduct = "";
	}

	public static void exit() {
		socket.close();
	}

	private static void onMessage(String message) {
		JSONObject obj = new JSONObject(message);

		// ignore any messages that aren't a price update
		if (!obj.getString("type").equals("ticker")) return;

		// otherwise, get the price info and call the update callback
		BigDecimal price = new BigDecimal(obj.getString("price"));
		BigDecimal bid = new BigDecimal(obj.getString("best_bid"));
		BigDecimal ask = new BigDecimal(obj.getString("best_ask"));
		BigDecimal spread = ask.subtract(bid);

		updateCallback.accept(new Quote(price, bid, ask, spread));
	}
}
