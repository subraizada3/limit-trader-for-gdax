/*
 * Copyright 2018 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.trader.util;

import com.subraizada.trader.Trader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

// class to deal with API keys for GDAX
public class ApiGdaxKey {
	private static String key, secret, passphrase;

	public static void initialize() throws IOException {
		String keyFilePath;
		if (Trader.TEST_MODE) {
			keyFilePath = System.getProperty("user.home") + "/.limitTraderKeyTest";
		} else {
			keyFilePath = System.getProperty("user.home") + "/.limitTraderKey";
		}

		File keyFile = new File(keyFilePath);
		BufferedReader br = new BufferedReader(new FileReader(keyFile));
		key = br.readLine().substring(7); // APIKEY=xxx
		secret = br.readLine().substring(10); // APISECRET=xxx
		passphrase = br.readLine().substring(14); // APIPASSPHRASE=xxx
	}

	public static String getKey() {
		return key;
	}

	public static String getSecret() {
		return secret;
	}

	public static String getPassphrase() {
		return passphrase;
	}
}
