/*
 * Copyright 2018 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.trader.util;

public class Formatting {
	// clip a string to this number of decimal places, without rounding
	public static String toDecimalPlacesString(String s, int numPlaces) {
		if (s.contains(".")) {
			int periodPos = s.indexOf(".");
			int placesAfterDecimal = s.length() - periodPos - 1;

			if (placesAfterDecimal == numPlaces) {
				return s;
			} else if (placesAfterDecimal <= numPlaces) {
				// call this method again with just the whole number part, to add the decimal and 0s
				return toDecimalPlacesString(s.substring(0, periodPos), numPlaces);
			} else {
				// trim the string, without rounding
				return s.substring(0, periodPos + numPlaces + 1);
			}
		} else {
			// add the . and 0s
			s += ".";

			StringBuilder sBuilder = new StringBuilder(s);
			for (int i = 0; i < numPlaces; i++) {
				sBuilder.append("0");
			}

			return sBuilder.toString();
		}
	}
}
