/*
 * Copyright 2018 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.subraizada.trader.controller;

import com.subraizada.trader.Trader;
import com.subraizada.trader.model.RootModel;
import com.subraizada.trader.util.ApiGdaxKey;
import com.subraizada.trader.util.ApiGdaxRest;
import com.subraizada.trader.util.ApiGdaxWebSocket;
import com.subraizada.trader.util.Currencies;
import com.subraizada.trader.util.Formatting;
import com.subraizada.trader.util.Quote;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class RootViewController {

	// TODO: clean up the code, return values from api buy/sell
	// TODO: account updating stops

	@FXML
	private VBox productSelectionBox;
	@FXML
	private RadioButton marketOrderButton, limitOrderButton;
	@FXML
	private Label price, bid, ask, spread;
	@FXML
	private Label currency1id, currency2id, currency1amount, currency2amount;
	@FXML
	private Button cancelAllOrders;

	private RootModel model;

	private HashMap<KeyCode, Instant> doubleTapEvents = new HashMap<>(6, 1);

	@FXML
	private void initialize() {
		model = new RootModel();

		// allow UI to load while init happens
		new Thread(this::lateInit).run();
	}

	private void lateInit() {
		try {
			// start APIs
			ApiGdaxKey.initialize();
			model.setTradingPairs(ApiGdaxRest.getProducts());
			ApiGdaxWebSocket.initialize(this::setPrices);
			model.setAccounts(ApiGdaxRest.getAccounts());

			// set labels in UI
			model.setCurrentProduct(Trader.INITIAL_TICKER);
			tradingPairButtonClicked(Trader.INITIAL_TICKER);
			setPrices(ApiGdaxRest.quote(model.getCurrentProduct()));

			ToggleGroup orderTypeGroup = new ToggleGroup();
			marketOrderButton.setToggleGroup(orderTypeGroup);
			limitOrderButton.setToggleGroup(orderTypeGroup);
		} catch (Exception e) {
			e.printStackTrace();
			Alert alert = new Alert(Alert.AlertType.ERROR, "Could not connect to GDAX API.");
			alert.showAndWait();
			Platform.exit();
		}

		Platform.runLater(() -> generateProductButtons(model.getTradingPairs()));

		// start services to update accounts and # of pending orders
		ScheduledExecutorService accountsService = Executors.newScheduledThreadPool(1);
		accountsService.scheduleAtFixedRate(this::updateAccountsServiceCallback, 1, 1, TimeUnit.SECONDS);
		ScheduledExecutorService ordersService = Executors.newScheduledThreadPool(1);
		ordersService.scheduleAtFixedRate(this::updateOrdersServiceCallback, 1, 1, TimeUnit.SECONDS);

		// add services to Trader class so they are shut down when program exits
		Trader.services.add(accountsService);
		Trader.services.add(ordersService);
	}

	private void generateProductButtons(ArrayList<String> tradingPairs) {
		// base products that are traded: BTC, BCH, ETC, LTC
		ArrayList<String> baseProducts = new ArrayList<>(4);

		// add base products to ArrayList
		for (String s : tradingPairs) {
			String pairBaseProduct = s.substring(0, 3);
			if (!baseProducts.contains(pairBaseProduct)) baseProducts.add(pairBaseProduct);
		}

		// for every base product, add a hbox to the vbox
		// also make a hashmap of new HBoxes to easily find them later
		HashMap<String, HBox> hBoxHashMap = new HashMap<>(4, 1);
		for (String s : baseProducts) {
			// add a HBox with a Label with the base product
			Label newLabel = new Label(s);
			newLabel.setTextOverrun(OverrunStyle.CLIP);
			newLabel.setPrefWidth(40);
			newLabel.setStyle("-fx-font-weight: bold");

			HBox newBox = new HBox(newLabel);
			newBox.setAlignment(Pos.CENTER);
			newBox.setSpacing(4);

			productSelectionBox.getChildren().add(newBox);
			hBoxHashMap.put(s, newBox);
		}

		// for every trading pair, add a button to trade it to its hbox
		// also add the buttons to a togglegroup so only one can be selected
		final ToggleGroup toggleGroup = new ToggleGroup();
		for (String s : tradingPairs) {
			HBox box = hBoxHashMap.get(s.substring(0, 3)); // get the hbox for this base pair
			// add a button to trade it with the other currency
			RadioButton b = new RadioButton(s.substring(4));
			// make it look like a normal button instead of a radio button - https://stackoverflow.com/a/33796837
			b.getStyleClass().add("toggle-button");

			b.setTextOverrun(OverrunStyle.CLIP);
			// add it to the toggle group
			b.setToggleGroup(toggleGroup);
			// set button ID to trading pair string
			b.setId(s);
			// set the current trading pair when clicked
			b.setOnAction(event -> tradingPairButtonClicked(((RadioButton) event.getSource()).getId()));
			// if this is the initially selected trading pair, select this button
			if (s.equals(Trader.INITIAL_TICKER)) b.setSelected(true);

			box.getChildren().add(b);
		}
	}

	private void tradingPairButtonClicked(String id) {
		// buttons for switching trading pair clicked
		// set trading pair IDs for APIs
		model.setCurrentProduct(id);
		ApiGdaxWebSocket.unsubscribeFromCurrentProduct();
		ApiGdaxWebSocket.subscribe(id);

		updateAccountsDisplay();

		// request price update now instead of waiting for next tick on websocket
		try {
			setPrices(ApiGdaxRest.quote(model.getCurrentProduct()));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// update product in Java Preferences API to load this on next startup
		Preferences.userNodeForPackage(Trader.class).put("product", model.getCurrentProduct());
	}

	private void updateAccountsDisplay() {
		// update currency labels
		String currency1 = model.getCurrentProduct().substring(0, 3),
				currency2 = model.getCurrentProduct().substring(4);
		Platform.runLater(() -> {
			currency1id.setText(currency1);
			currency2id.setText(currency2);
		});

		// and show the amount of each currency available to trade with
		HashMap<String, BigDecimal> accounts = model.getAccounts();
		currency1amount.setText(Formatting.toDecimalPlacesString(
				accounts.get(currency1).toPlainString(), Currencies.getDecimalPlaces(currency1)));
		currency2amount.setText(Formatting.toDecimalPlacesString(
				accounts.get(currency2).toPlainString(), Currencies.getDecimalPlaces(currency2)));
	}

	private void setPrices(Quote t) {
		Platform.runLater(() -> {
			int decimalPlaces = Currencies.getDecimalPlaces(model.getCurrentProduct().substring(4));

			price.setText(Formatting.toDecimalPlacesString(t.getPrice().toString(), decimalPlaces));
			bid.setText(Formatting.toDecimalPlacesString(t.getBid().toString(), decimalPlaces));
			ask.setText(Formatting.toDecimalPlacesString(t.getAsk().toString(), decimalPlaces));
			spread.setText(Formatting.toDecimalPlacesString(t.getSpread().toString(), decimalPlaces));
		});
	}

	@FXML
	private void buySellButtonHandler(ActionEvent event) {
		// extract order details from button fx:id, e.g. buy_50
		String sourceButtonId = ((Button) event.getSource()).getId();

		buySellEventHandler(sourceButtonId);
	}

	private void buySellEventHandler(String order) {
		// refresh balance in accounts and prices
		model.setAccounts(ApiGdaxRest.getAccounts());
		Quote q;
		try {
			q = ApiGdaxRest.quote(model.getCurrentProduct());
			setPrices(q);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		// get order details (in button fx:id, e.g. buy_50)
		String[] orderDetails = order.split("_");
		boolean buy = orderDetails[0].equals("buy");

		try {
			// comments use BTC-USD as an example
			if (model.isMakeMarketOrders()) {
				// If it's a market order, specify how much BTC or USD to spend in a BTC(1)-USD(2) trade
				// If buying, spending USD (2); if selling, spending BTC (1)
				// So select USD if buying, and BTC if selling
				String tradingCurrency = buy ? model.getCurrentProduct().substring(4) : model.getCurrentProduct().substring(0, 3);

				// Get the amount of that currency available to spend
				BigDecimal currencyAvailable = model.getAccounts().get(tradingCurrency);
				// Convert available currency to the amount of currency to put into the trade
				// Don't need to subtract fees for 100% orders
				if (!orderDetails[1].equals("100"))
					currencyAvailable = reduceNumberToOrderSize(currencyAvailable, tradingCurrency, orderDetails[1], "");
				ApiGdaxRest.orderMarket(model.getCurrentProduct(), buy, currencyAvailable);
			} else {
				// Limit order
				// For BTC-USD
				// When buying, give price in USD and amount in BTC to buy (derived from USD price)
				// When selling, give price in USD and amount in BTC to sell (derived from BTC available)
				String priceCurrency = model.getCurrentProduct().substring(4);
				String tradingCurrency;

				if (buy)
					tradingCurrency = priceCurrency; // when buying, price is in USD, and we're giving up USD
				else
					tradingCurrency = model.getCurrentProduct().substring(0, 3); // when selling, price is in USD, but we're giving up our BTC


				String feeMultiplier = "0.9975"; // 0.25% fee for BTC/BCH
				if (model.getCurrentProduct().contains("LTC") || model.getCurrentProduct().contains("ETH"))
					feeMultiplier = "0.997"; // 0.3% fee for LTC/ETH

				if (!buy) { // sell: amount of BTC is 100/50/25% of available BTC; sell at bid + minimum increment

					BigDecimal currencyAvailable = model.getAccounts().get(tradingCurrency);
					// same as with market order, reduce to 25/50/99.75%
					currencyAvailable = reduceNumberToOrderSize(currencyAvailable, tradingCurrency, orderDetails[1], feeMultiplier);
					ApiGdaxRest.orderLimit(model.getCurrentProduct(), false, currencyAvailable, changeByMinimumIncrement(q.getBid(), priceCurrency, true));

				} else { // buy: calculate price, calculate amount of BTC that can be bought at that price; buy at ask - min

					BigDecimal price = changeByMinimumIncrement(q.getAsk(), priceCurrency, false);
					BigDecimal currencyAvailable = model.getAccounts().get(tradingCurrency);
					currencyAvailable = reduceNumberToOrderSize(currencyAvailable, tradingCurrency, orderDetails[1], feeMultiplier);

					// calculate how much BTC we can buy at this price, and as always, round down
					// priceCurrency and tradingCurrency are both USD, but we're using BTC here
					BigDecimal btcToBuy = currencyAvailable.divide(price,
							Currencies.getDecimalPlaces(model.getCurrentProduct().substring(0, 3)),
							RoundingMode.DOWN);

					ApiGdaxRest.orderLimit(model.getCurrentProduct(), true, btcToBuy, price);
				}
			}
		} catch (Exception e) {
			// don't print stack trace
		}
	}

	private static BigDecimal reduceNumberToOrderSize(BigDecimal totalAvailable, String currency, String desiredOrderSize, String feeMultiplier) {
		if (desiredOrderSize.equals("100")) {
			return totalAvailable.multiply(new BigDecimal(feeMultiplier))
					.setScale(Currencies.getDecimalPlaces(currency), RoundingMode.DOWN);
		} else {
			return totalAvailable.multiply(new BigDecimal("0." + desiredOrderSize)) // "0." + "50" = "0.50"
					.setScale(Currencies.getDecimalPlaces(currency), RoundingMode.DOWN);
		}
	}

	private BigDecimal changeByMinimumIncrement(BigDecimal price, String priceCurrency, boolean increase) {
		BigDecimal changeAmount;

		if (Currencies.getDecimalPlaces(priceCurrency) == 2) // 0.01 for fiats, 0.00001 for cryptos
			changeAmount = new BigDecimal("0.01");
		else changeAmount = new BigDecimal("0.00001");

		if (increase) return price.add(changeAmount);
		else return price.subtract(changeAmount);
	}

	@FXML
	private void switchToOrderTypeMarket() {
		model.setMakeMarketOrders(true);
	}

	@FXML
	private void switchToOrderTypeLimit() {
		model.setMakeMarketOrders(false);
	}

	@FXML
	private void cancelAllOrders() {
		ApiGdaxRest.cancelAllOrders();
	}

	private void updateCancelAllOrdersButton(int numOrders) {
		if (numOrders == -1) {
			cancelAllOrders.setText("GDAX API Unavailable!");
		} else {
			cancelAllOrders.setText("Cancel all " + numOrders + " orders");
		}
	}

	private void updateAccountsServiceCallback() {
		model.setAccounts(ApiGdaxRest.getAccounts());
		Platform.runLater(this::updateAccountsDisplay);
	}

	private void updateOrdersServiceCallback() {
		int numOrders = ApiGdaxRest.getPendingOrders();
		Platform.runLater(() -> updateCancelAllOrdersButton(numOrders));
	}

	public void setKeybindings() {
		// add keyboard shortcuts
		Scene scene = productSelectionBox.getScene();
		scene.setOnKeyPressed(event -> {
			switch (event.getCode()) {
				case C:
					cancelAllOrders();
					break;
				case M:
					marketOrderButton.setSelected(true);
					switchToOrderTypeMarket();
					break;
				case L:
					limitOrderButton.setSelected(true);
					switchToOrderTypeLimit();
					break;
				case Q:
				case W:
				case E:
				case A:
				case S:
				case D:
					doubleTapCheck(event.getCode());
					break;
			}
		});
	}

	private void doubleTapCheck(KeyCode keyCode) {
		// handle double tap key events for QWE/ASD

		Instant lastPressTime = doubleTapEvents.get(keyCode);

		// if this is the first time the key has been pressed, but current time as last press time
		if (lastPressTime == null) {
			doubleTapEvents.put(keyCode, Instant.now());
		} else {
			Instant now = Instant.now();

			// difference between last press and now
			Duration between = Duration.between(lastPressTime, now);

			// if it's less than a second, fire a doubletap event
			if (between.getSeconds() == 0) { // <1 second elapsed
				doubleTapEvent(keyCode);
			}

			// and either way, set the last press time to now
			doubleTapEvents.put(keyCode, now);
		}
	}

	private void doubleTapEvent(KeyCode keyCode) {
		// execute a buy/sell event
		// generate the order id based off the keyCode, e.g. buy_50 for Buy 50% button = W
		String orderId = "";
		switch (keyCode) {
			case Q:
				orderId = "buy_100";
				break;
			case W:
				orderId = "buy_50";
				break;
			case E:
				orderId = "buy_25";
				break;
			case A:
				orderId = "sell_100";
				break;
			case S:
				orderId = "sell_50";
				break;
			case D:
				orderId = "sell_25";
				break;
		}
		buySellEventHandler(orderId);
	}
}
