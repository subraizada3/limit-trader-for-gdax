package com.subraizada.trader_setup.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class RootController {
	@FXML
	private TextField fieldKey, fieldSecret, fieldPassphrase;
	@FXML
	private Label feedbackLabel;

	@FXML
	private void save() {
		String keyFilePath = System.getProperty("user.home") + "/.limitTraderKey";

		File keyFile = new File(keyFilePath);
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(keyFile)));
			bw.write("APIKEY=" + fieldKey.getText() + "\n");
			bw.write("APISECRET=" + fieldSecret.getText() + "\n");
			bw.write("APIPASSPHRASE=" + fieldPassphrase.getText() + "\n");
			bw.flush();
			bw.close();

			feedbackLabel.setStyle("-fx-text-fill: green");
			feedbackLabel.setText("Saved!");
			new Thread(this::clearFeedbackLabel).start();

		} catch (IOException e) {
			feedbackLabel.setStyle("-fx-text-fill: green");
			feedbackLabel.setText("Save failed!");
			new Thread(this::clearFeedbackLabel).start();

			e.printStackTrace();
		}
	}

	private void clearFeedbackLabel() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Platform.runLater(() -> feedbackLabel.setText(""));
	}
}
