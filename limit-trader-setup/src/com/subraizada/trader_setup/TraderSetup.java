package com.subraizada.trader_setup;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TraderSetup extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("view/RootView.fxml"));
		primaryStage.setTitle("Limit Trader Setup");
		primaryStage.setScene(new Scene(root));
		primaryStage.setResizable(false);
		primaryStage.sizeToScene();
		primaryStage.show();
	}


	public static void main(String[] args) {
		launch(args);
	}
}
